package com.gitlab.semanticweb1819

import com.gitlab.semanticweb1819.parser.c.CCodeParser
import com.gitlab.semanticweb1819.parser.c.model.MapperVisitorWithMemory
import com.gitlab.semanticweb1819.rdf.COntology
import com.gitlab.semanticweb1819.rdf.serialization.SerializationUtils
import com.gitlab.semanticweb1819.rdf.serialization.mapping.ASTInfoMapping
import mu.KotlinLogging
import org.apache.jena.rdf.model.ModelFactory
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

private val logger = KotlinLogging.logger {}

fun main(args: Array<String>) {
    val outputModel = ModelFactory.createDefaultModel()

    if (args[0].isBlank() || args[1].isBlank()) {
        logger.error { "First argument should be the path to the file to parse" }
        logger.error { "Second argument should be the path to the output file" }
        System.exit(1)
    }

    val toParse: String? = try {
        Files.readString(Paths.get(args[0]))
    } catch (e: Exception) {
        logger.error(e) { "Cannot read file ${args[0]}" }
        System.exit(1)
        null
    }

    val outputStream = try {
        File(args[1]).outputStream()
    } catch (e: Exception) {
        logger.error(e) { "Cannot write output file ${args[1]}" }
        System.exit(1)
        null
    }

    val ast = CCodeParser.parse(toParse!!)

    ast.translationUnit.accept(
            MapperVisitorWithMemory(StringBuilder(SerializationUtils.CREATED_RESOURCES_NAMESPACE))
            { memory, currentNode ->
                ASTInfoMapping.toRDF(memory, currentNode, outputModel)
            })

    outputModel.setNsPrefixes(COntology.prefixes)
    outputModel.write(outputStream!!)

    logger.info { "Created resulting file at ${args[1]}" }
}
