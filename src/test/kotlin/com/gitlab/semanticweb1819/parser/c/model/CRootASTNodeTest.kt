package com.gitlab.semanticweb1819.parser.c.model

import com.gitlab.semanticweb1819.parser.c.CCodeParser
import io.kotlintest.Spec
import io.kotlintest.matchers.collections.shouldContainExactly
import io.kotlintest.matchers.collections.shouldContainExactlyInAnyOrder
import io.kotlintest.matchers.collections.shouldHaveSize
import io.kotlintest.specs.StringSpec

/**
 * Test class for [CRootASTNode]
 */
internal class CRootASTNodeTest : StringSpec() {

    private val code = """
        #include<stdio.h>
        #include<math.h>
        #define Integer int
        int main() { return 0; }
    """.trimIndent()

    private lateinit var astRoot: CRootASTNode
    override fun beforeSpec(spec: Spec) {
        astRoot = CCodeParser.parse(code)
    }

    init {
        "All includes are present"{
            astRoot.includeDirectives shouldHaveSize 2
            astRoot.includeDirectives.map { it.name.toString() }
                    .shouldContainExactlyInAnyOrder("stdio.h", "math.h")
        }

        "All defines are present"{
            astRoot.macroDefinitions shouldHaveSize 1
            astRoot.macroDefinitions.map { it.name.toString() to it.expansion.toString() }
                    .shouldContainExactly("Integer" to "int")
        }
    }
}
