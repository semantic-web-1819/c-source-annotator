package com.gitlab.semanticweb1819.parser.c

import com.gitlab.semanticweb1819.test.utils.resources.CodeSamples
import io.kotlintest.properties.forAll
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.StringSpec
import mu.KotlinLogging
import java.nio.file.Files

private val logger = KotlinLogging.logger {}

/**
 * A class to test [CCodeParser]
 */
internal class CCodeParserTest : StringSpec({

    "parsing simple code"{
        val exampleCode = """
            #include<stdio.h>
            #define f ff
            int f(int* param);
            int main(){
                int a = 1;
                return f(&a) + 5;
            }
            int f(int* p){
                *p = 22;
                return 5;
            }
            """.trimIndent()

        val translationUnit = CCodeParser.parse(exampleCode).translationUnit
        val astNodes = sequenceOf(
                *translationUnit.includeDirectives,
                *translationUnit.macroDefinitions,
                *translationUnit.children)

        val parsedString = astNodes.mapNotNull { it?.rawSignature }.joinToString(separator = "\n")
        parsedString shouldBe exampleCode
    }

    "parsing correct code"{
        forAll(CodeSamples()) { codeSamplePath ->
            val codeSample = Files.readString(codeSamplePath)
            logger.debug { "Parsing file: ${codeSamplePath.fileName}" }
            val translationUnit = CCodeParser.parse(codeSample).translationUnit
            val astNodes = sequenceOf(
                    *translationUnit.includeDirectives,
                    *translationUnit.macroDefinitions,
                    *translationUnit.children)

            astNodes.toList().isNotEmpty()
        }
    }

    "parsing incorrect code"{
        val incorrectCode = """
                int main(){
                    return 0
                }
            """.trimIndent()

        shouldThrow<IllegalArgumentException> { CCodeParser.parse(incorrectCode) }
    }
})
