package com.gitlab.semanticweb1819.test.utils.resources

import io.kotlintest.properties.Gen
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.streams.toList

/**
 * A class to load test resources
 */
internal class CodeSamples : Gen<Path> {
    private val correctSamplesFolder = "c/correctsamples/"
    private val samplesFolder: Path = Paths.get(javaClass.classLoader.getResource(correctSamplesFolder).toURI())

    override fun constants(): Iterable<Path> {
        return Files.list(samplesFolder).toList()
    }

    override fun random(): Sequence<Path> = emptySequence()
}